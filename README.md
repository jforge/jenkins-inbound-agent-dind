# jenkins-inbound-dind
A jenkins slave image with built-in docker-in-docker capabilities

## Why?
The primary goal of this image is to allow users to build docker images from within containerized Jenkins agents. This includes agents running on Kubernetes or Docker Swarm clusters.

## Tags
- **latest**: Vanilla [jenkins/inbound-agent](https://hub.docker.com/r/jenkins/inbound-agent)  image with `docker` added to it.
- **awscli**: Vanilla [jenkins/inbound-agent](https://hub.docker.com/r/jenkins/inbound-agent) with `docker` and `awscli` added to it.
- **awscli-kubectl**: Vanilla [jenkins/inbound-agent](https://hub.docker.com/r/jenkins/inbound-agent) with `docker`, `kubectl` and `awscli` added to it.

## How to use this
Simply use the `diegolima/jenkins-inbound-dind` image for your agents, mount the `/var/run/docker.sock` file inside the slave container and use `sudo docker` to build/run images and containers.

Note that sudo is necessary to overcome permission issues with the socket file. This image is configured as such the jenkins user can **only** use the docker command as root.

### Sample Jenkins pod template configuration

![Pod template configuration](sample_jenkins_pod_template.png)

### Sample Jenkins pipeline script
```
podTemplate(cloud: 'my-kubernetes-cloud-name') {
    node('jenkins-slave') {
        container('jenkins-slave'){
            stage('Run Test') {
                sh '''
                    set +x
                    echo Node name: $NODE_NAME
                    echo Jenkins server: $JENKINS_URL
                    echo IP address: $(curl -s https://api.ipify.org)
                    date
                    sudo docker version
                    echo ""
                '''
            }
        }
    }
}
```


## How often is the image updated?
Daily builds are done in the best effort to keep the image in-sync with the latest [jenkins/inbound-agent](https://hub.docker.com/r/jenkins/inbound-agent) available. For the moment builds are all based on the `latest` tag. If you'd like other tags feel free to contact me and I'll add them to the build process.

## Where can I find the Dockerfile for this?
[Here](https://gitlab.com/diegolima/jenkins-inbound-agent-dind)
